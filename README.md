# Water Level iOS

## About the app

The Wadden Water Level app provides a simple interface to the data provided by the Rijkswaterstaat API.
The goal of the app is to provide acuarate information in a clear way.
It is important to note that the app is intended as an additional tool to makes sailing on the Wadden Sea easier.
It is not intended to replace other means of navigation. Always double check your data before starting your journey. 
If in doubt contact the local authorities.

### Data origin
The app uses Rijkswaterstaat API's to provide up to data data. 
* For more information see their website: https://rijkswaterstaatdata.nl/data-zoeken/
* The API's are described here: https://rijkswaterstaatdata.nl/waterdata/
* A developer description can be found here: https://rijkswaterstaat.github.io/wm-ws-dl/#introduction
* They also have a nice user interface to examine their data here: https://www.rijkswaterstaat.nl/water/waterdata-en-waterberichtgeving/waterdata

### Data usage
The water level as shown is delivered by the Rijkswaterstaat API as a ten minute average. 
This means that the water level of 9:10 is calculated based on the measurements taken from 9:05 until 9:15 and will be available a (few) minutes later

### Roadmap
This app is by design a very basic app. Although the intention is to keep the app basic, there are plenty of idea's for functionality to add.
* Improve UI to better function in dark mode
* Add translations (NL, EN and DU)
* Add setting menu to select other locations
* Add a widget
* Add a smartphone widget
* Add a graph of previous hours
* Add the astronomical and deviation data
* Improve refresh interface (pull down with nice animation)

## Terms & Conditions
The app terms of use can be found here: [terms and conditions](termsandconditions.md)

## Privacy Policy
The app privacy policy can be found here: [privacy](privacy.md)

## Contact
You can contact me at: waddenwaterlevel@gmail.com