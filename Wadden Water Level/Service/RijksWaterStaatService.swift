//
//  RijkswaterstaatService.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 20/05/2023.
//

import Foundation

struct ExpectedWaterData: Identifiable {
    let id = UUID()
    var waterLevel: Int
    var atDateTime: Date
}

enum LocationError: Error {
    case locationNotFound(String)
}

class RijksWaterStaatService {
    let rwsLocations: [Location:RWSLocation] = [
        .nes:RWSLocation(X:683288.335482817, Y:5923648.18977766, Code:"NES"),
        .eemshaven:RWSLocation(X:754251.664251634, Y:5928910.1372502, Code:"EMSH"),
        .denhelder:RWSLocation(X:617120.700586658, Y:5869664.28145237, Code:"DENH"),
        .denoever:RWSLocation(X:637382.403593524, Y:5866830.02723899, Code:"OEBU"),
        .delfzijl:RWSLocation(X:761913.320818121, Y:5915831.92602204, Code:"DLFZ"),
        .harlingen:RWSLocation(X:661156.301158369, Y:5894527.94615404, Code:"HARL"),
        .huibertgat:RWSLocation(X:724996.335864394, Y:5941470.2703589, Code:"HUIB"),
        .hoekvanholland: RWSLocation(X:576917.675576278, Y:5759136.13463449, Code:"HOEK"),
        .holwerd:RWSLocation(X:691227.709999519, Y:5920843.20888694, Code:"HOLW"),
        .ijmuiden:RWSLocation(X: 605632.956827924, Y: 5813598.08945097, Code: "IJMH"),
        .kornwerderzand:RWSLocation(X:656430.583586584, Y:5882780.82203517, Code:"KOBU"),
        .lauwersoog:RWSLocation(X:712485.362427151, Y:5922496.96218518, Code:"LAUW"),
        .oudeschild:RWSLocation(X:624112.288320664, Y:5878437.18294564, Code:"OUDE"),
        .scheveningen:RWSLocation(X:586551.020314752, Y:5772806.49292637, Code:"SCHE"),
        .schiermonnikoog:RWSLocation(X:712630.911327752, Y:5929208.21009008, Code:"SCHI"),
        .vlieland:RWSLocation(X:639430.974073676, Y:5907184.533288, Code:"VLIE"),
        .vlissingen:RWSLocation(X:541518.745919649, Y:5699254.96425966, Code:"VLIS"),
        .westterschelling:RWSLocation(X:647684.020116895, Y:5914923.77870158, Code:"WTER"),
        .wierumergronden:RWSLocation(X:696130.296598613, Y:5933727.14138876, Code:"WIER"),
    ]
    
    func getCurrentWaterLevel(forLocation: Location) async throws -> ExpectedWaterData {
        let requiredLocation = rwsLocations[forLocation]!
        
        let rwsWaterData = try await RijksWaterStaatClient().getCurrentWaterLevel(forLocation: requiredLocation)
        
        let waterLevel = String(Int(rwsWaterData.WaarnemingenLijst[0].MetingenLijst[0].Meetwaarde.Waarde_Numeriek))
        let timestamp = rwsWaterData.WaarnemingenLijst[0].MetingenLijst[0].Tijdstip
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        let measureTime = dateFormatter.date(from: timestamp)!
        //                let printFormatter = DateFormatter()
        //                printFormatter.setLocalizedDateFormatFromTemplate("dd MMM HH:mm")
        //                let readingDate = printFormatter.string(from: measureTime)
                
        return ExpectedWaterData(waterLevel: Int(waterLevel)!, atDateTime: measureTime)
    }
    
    func getExpectedWaterLevel(forLocation: Location) async throws -> [ExpectedWaterData] {
        let requiredLocation = rwsLocations[forLocation]!
        
        let start = Date.now.addingTimeInterval(-5400)
        let end = Date.now.addingTimeInterval(32400)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        
        let rwsWaterData = try await RijksWaterStaatClient().getExpectedWaterLevel(forLocation: requiredLocation,
                                                                                   startDate: dateFormatter.string(from: start),
                                                                                   endDate: dateFormatter.string(from: end))
        
        let expectedWaterLevels = rwsWaterData.WaarnemingenLijst[0].MetingenLijst.map { measurement in
            let waterLevel = String(Int(measurement.Meetwaarde.Waarde_Numeriek))
            let timestamp = measurement.Tijdstip
            let measureTime = dateFormatter.date(from: timestamp)!
            //                    print("Expected level \(waterLevel) at \(timestamp)")
            return ExpectedWaterData(waterLevel: Int(waterLevel)!, atDateTime: measureTime)
        }
        
        let endOfData = min(63, expectedWaterLevels.count) - 1
        
        return Array(expectedWaterLevels[0...endOfData])
    }
    
    func getAstroWaterLevel(forLocation: Location) async throws -> [ExpectedWaterData] {
        let requiredLocation = rwsLocations[forLocation]
        
        let start = Date.now.addingTimeInterval(-5400)
        let end = Date.now.addingTimeInterval(32400)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"

        let rwsWaterData = try await RijksWaterStaatClient().getAstroWaterLevel(forLocation: requiredLocation!,
                                                             startDate: dateFormatter.string(from: start),
                                                             endDate: dateFormatter.string(from: end))
        
        let expectedWaterLevels = rwsWaterData.WaarnemingenLijst[0].MetingenLijst.map { measurement in
            let waterLevel = String(Int(measurement.Meetwaarde.Waarde_Numeriek))
            let timestamp = measurement.Tijdstip
            let measureTime = dateFormatter.date(from: timestamp)!
//                    print("Astro level \(waterLevel) at \(timestamp)")
            return ExpectedWaterData(waterLevel: Int(waterLevel)!, atDateTime: measureTime)
        }
        
        let endOfData = min(63, expectedWaterLevels.count) - 1
        
        return Array(expectedWaterLevels[0...endOfData])
    }
    
    func getPreviousWaterLevel(forLocation: Location) async throws -> [ExpectedWaterData] {
//        guard let requiredLocation = rwsLocations[forLocation] else {
//            print("Could not find matching RWS location for selection: " + forLocation.rawValue)
//            completion(.failure(LocationError.locationNotFound("Could not find matching RWS location for selection")))
//            return
//        }
        
        let requiredLocation = rwsLocations[forLocation]!
        
        let start = Date.now.addingTimeInterval(-10800)
        let end = Date.now
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        
        let rwsWaterData = try await RijksWaterStaatClient().getPreviousWaterLevel(forLocation: requiredLocation,
                                                                                   startDate: dateFormatter.string(from: start),
                                                                                   endDate: dateFormatter.string(from: end))
        let expectedWaterLevels = rwsWaterData.WaarnemingenLijst[0].MetingenLijst.map { measurement in
            let waterLevel = String(Int(measurement.Meetwaarde.Waarde_Numeriek))
            let timestamp = measurement.Tijdstip
            let measureTime = dateFormatter.date(from: timestamp)!
//                                print("Previous level \(waterLevel) at \(timestamp)")
            return ExpectedWaterData(waterLevel: Int(waterLevel)!, atDateTime: measureTime)
        }
        
        let endOfData = min(18, expectedWaterLevels.count) - 1
        
        return Array(expectedWaterLevels[0...endOfData])
    }
}
