//
//  SettingsService.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 21/05/2023.
//

import Foundation

class SettingsService {
    func getSettings() -> Settings {
        guard let selectedLocationString = UserDefaults.standard.string(forKey: "SelectedLocation") else {
            return Settings(selectedLocation: .lauwersoog)
        }
        guard let selectedLocation = Location(rawValue: selectedLocationString) else {
            // Build 20 had the enum value .ameland, this has been changed in build 21 to .nes
            // Therefore it makes sense that the fallback would be .nes in cases we cannot instantiate
            // the enum value from the rawValue
            return Settings(selectedLocation: .nes)
        }
        
        return Settings(selectedLocation: selectedLocation)
    }
}
