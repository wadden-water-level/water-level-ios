//
//  SettingsView.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 19/05/2023.
//

import SwiftUI

enum Location: String, CaseIterable, Equatable {
    case nes, delfzijl, denhelder, denoever, eemshaven, harlingen, hoekvanholland, holwerd, huibertgat, ijmuiden, kornwerderzand, lauwersoog,  oudeschild, scheveningen, schiermonnikoog, vlieland, vlissingen, westterschelling, wierumergronden

    var localizedName: LocalizedStringKey { LocalizedStringKey(rawValue) }
}

struct Settings {
    var selectedLocation: Location
}

struct SettingsView: View {
    @Binding var settings: Settings
    @State private var selectedLocation: Location = .lauwersoog

    @Environment(\.presentationMode) var presentationMode

    init(settings: Binding<Settings>) {
        _settings = settings
        _selectedLocation = State(initialValue: settings.wrappedValue.selectedLocation)
    }
    
    var body: some View {
        VStack {
            Form {
                Section {
                    Picker("Locations", selection: $selectedLocation) {
                        ForEach(Location.allCases, id: \.self) { value in
                            Text(value.localizedName).tag(value)
                        }
                    }.onChange(of: selectedLocation) { newValue in
                        settings.selectedLocation = newValue
                        UserDefaults.standard.set(newValue.rawValue, forKey: "SelectedLocation")
                        presentationMode.wrappedValue.dismiss()
                    }
                }
            }
        }

    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(settings: .constant(Settings(selectedLocation: .lauwersoog)))
    }
}
