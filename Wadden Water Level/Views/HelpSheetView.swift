//
//  HelpSheetView.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 03/05/2023.
//

import SwiftUI

struct HelpSheetView: View {
    @Environment(\.dismiss) var dismiss

    var body: some View {
        ZStack {
            HStack {
                Text("About this app").font(.title).fontWeight(.bold)
            }
            HStack {
                Spacer()
                Button {
                    dismiss()
                } label: {
                    Image(systemName: "xmark.circle").imageScale(.large)
                }
                .padding(.trailing)
            }
        }.padding(.top)
        ScrollView {
            VStack(alignment: .trailing) {
                VStack(alignment: .leading, spacing: 0) {
                    Text("Instructions").bold().padding()
                    Text("User instructions").padding()
                    Text("Example").bold().padding()
                    Text("Water level example").padding()
                    Text("Rijkswaterstaat data").bold().padding()
                    Text("Data explenation").padding()
                    Text("More info").bold().padding()
                    Text("More info about water levels").padding()
                    Spacer()
                }
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                minHeight: 0,
                maxHeight: .infinity,
                alignment: .leading
            )
        }
    }
}

struct HelpSheetView_Previews: PreviewProvider {
    static var previews: some View {
        HelpSheetView()
    }
}
