//
//  ContentView.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 12/06/2021.
//

import SwiftUI
import Charts

struct ContentView: View {
    @State private var shouldDisplayDisclaimer = !UserDefaults.standard.bool(forKey: "DisclaimerShown")

    @State private var waterLevel = -9999
    @State private var deviation = "- -"
    @State private var currentWaterLevel = ExpectedWaterData(waterLevel: -9999, atDateTime: Date())
    @State private var expectedWaterLevel = [ExpectedWaterData]()
    @State private var previousWaterLevel = [ExpectedWaterData]()
    @State private var astroWaterLevel = [ExpectedWaterData]()
    @State private var readingDate = ""
    @State private var refreshState = LocalizedStringKey("")
    @State private var showingHelp = false
    @State private var showingSettings = false
    @State private var runningInTheForeground = true

    @State private var settings: Settings = SettingsService().getSettings()
    
    let timer = Timer.publish(every: 60, on: .main, in: .common).autoconnect()

    @Environment(\.scenePhase) var scenePhase
    @Environment(\.openURL) private var openURL
        
    var body: some View {
        NavigationStack {
            VStack {
                ZStack {
                    HStack {
                        NavigationLink(destination: SettingsView(settings: $settings)) {
                            Image(systemName: "list.bullet").imageScale(.large)
                        }.padding(.leading)
                        Spacer()
                    }
                    HStack {
                        Text(settings.selectedLocation.localizedName)
                            .font(.title)
                            .fontWeight(.bold)
                            .padding()
                    }
                    HStack {
                        Spacer()
                        Button {
                            showingHelp.toggle()
                        } label: {
                            Image(systemName: "info.circle").imageScale(.large)
                        }
                        .padding(.trailing)
                    }
                }.padding(.top, -15)
                
                Text("Water level")
                    .font(.title2).padding(.top)
                HStack {
                    waterLevel < 0 ? Text(" ") : Text("+ ").fontWeight(.bold)
                    waterLevel == -9999 ? Text("- -") : Text(String(waterLevel)).fontWeight(.bold)
                    Text("cm").fontWeight(.bold)
                }.font(.largeTitle).foregroundColor(Color.red).padding(.bottom)

                Text("Deviation")
                    .font(.title2).padding(.top)
                HStack {
                    deviation.starts(with: "-") ? Text(" ") : Text("+ ").fontWeight(.bold)
                    Text(deviation).fontWeight(.bold)
                    Text("cm").fontWeight(.bold)
                }.font(.largeTitle).foregroundColor(Color.red).padding(.bottom)
                
//                TideChart(previous: previousWaterLevel, expected: expectedWaterLevel, astro: astroWaterLevel)

//                TideChartDataView(data: [
//                    TideCartData(name: "previous", levels: self.previousWaterLevel),
//                    TideCartData(name: "expected", levels: self.expectedWaterLevel),
//                    TideCartData(name: "astro", levels: self.astroWaterLevel)
//                ])
                
                Text("Measurement time: \(readingDate)")
                Spacer()
                Text(refreshState)
            }.task() {
                await refresh()
            }.onReceive(timer) { _ in
                if(currentWaterLevel.atDateTime < (Date() - 15 * 60) && runningInTheForeground) {
                    Task {
                        await refresh()
                    }
                }
            }.alert(isPresented: $shouldDisplayDisclaimer) {
                Alert(title: Text("Terms of use"), message: Text("Short terms"), primaryButton: .default(Text("Confirm")) {
                    UserDefaults.standard.set(true, forKey: "DisclaimerShown")
                }, secondaryButton: .default(Text("Terms of use")) {
                    openURL(URL(string: "https://gitlab.com/wadden-water-level/water-level-ios/-/blob/main/termsandconditions.md")!)
                })
            }.sheet(isPresented: $showingHelp) {
                HelpSheetView()
            }.onChange(of: scenePhase) { newPhase in
                switch newPhase {
                    case .background: runningInTheForeground = false
                    case .active: 
                        runningInTheForeground = true
                        Task {
                            await refresh()
                        }
                    default: break
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension ContentView {
    func getWaterData() async -> ExpectedWaterData {
        do {
            let waterData = try await RijksWaterStaatService().getCurrentWaterLevel(forLocation: self.settings.selectedLocation)
            let printFormatter = DateFormatter()
            printFormatter.setLocalizedDateFormatFromTemplate("HH:mm")
            let readingDate = printFormatter.string(from: waterData.atDateTime)

            DispatchQueue.main.async {
                self.currentWaterLevel = waterData
                self.waterLevel = waterData.waterLevel
                self.readingDate = readingDate
            }
            
            return waterData
        } catch {
            print("getWaterData error do nothing")
            DispatchQueue.main.async {
                self.refreshState = "Failure loading data"
            }
        }
        return ExpectedWaterData(waterLevel: -9999, atDateTime: Date())
    }

    func getExpectedWaterData() async {
        do {
            let waterData = try await RijksWaterStaatService().getExpectedWaterLevel(forLocation: self.settings.selectedLocation)
            
            DispatchQueue.main.async {
                self.expectedWaterLevel = waterData
            }
        } catch {
            print("getExpectedWaterData error do nothing")
            DispatchQueue.main.async {
                self.refreshState = "Failure loading data"
            }
        }
    }

    func getPreviousWaterData() async {
        do {
            let waterData = try await RijksWaterStaatService().getPreviousWaterLevel(forLocation: self.settings.selectedLocation)
            
            DispatchQueue.main.async {
                self.previousWaterLevel = waterData
            }
        } catch {
            print("getPreviousWaterData error do nothing")
            DispatchQueue.main.async {
                self.refreshState = "Failure loading data"
            }
        }
    }

    func getAstroWaterData() async -> [ExpectedWaterData] {
        do {
            let waterData = try await RijksWaterStaatService().getAstroWaterLevel(forLocation: self.settings.selectedLocation)
            
            DispatchQueue.main.async {
                self.astroWaterLevel = waterData
            }
            
            return waterData
        } catch {
            print("getAstroWaterData error do nothing")
            DispatchQueue.main.async {
                self.refreshState = "Failure loading data"
            }
        }
        
        return [ExpectedWaterData]()
    }

    func calculateDeviation(current: ExpectedWaterData, astro: [ExpectedWaterData]) {
        astro.forEach { expected in
            if(current.waterLevel != -9999) {
                let difference = current.waterLevel - expected.waterLevel
                if(expected.atDateTime == current.atDateTime) {
                    DispatchQueue.main.async {
                        self.deviation = String(difference)
                    }
                    return
                }
            }
        }
    }
    
    func refresh() async {
        print("refresh")
        DispatchQueue.main.async {
            self.refreshState = LocalizedStringKey("Loading...")
        }
        await getExpectedWaterData()
        print("expected")
        await getPreviousWaterData()
        print("previous")
        let astro = await getAstroWaterData()
        print("astr")
        let current = await getWaterData()
        print("current")
        calculateDeviation(current: current, astro: astro)
        
        let lastCheck = currentWaterLevel.atDateTime
        let now = Date()
        
        let nextCheck = if(now > (lastCheck + 16 * 60)) {
            now + 60
        } else {
            lastCheck + 16 * 60
        }

        let printFormatter = DateFormatter()
        printFormatter.setLocalizedDateFormatFromTemplate("HH:mm")
        let nextCheckTime = printFormatter.string(from: nextCheck)

        DispatchQueue.main.async {
            self.refreshState = LocalizedStringKey("Next update at: \(nextCheckTime)")
        }
        print("finished")
    }
}

struct TideChart: View {
    let previous: [ExpectedWaterData]
    let expected: [ExpectedWaterData]
    let astro: [ExpectedWaterData]
    
    var body: some View {
        Chart {
            ForEach(previous) { data in
                LineMark(x: .value("Time", data.atDateTime),
                         y: .value("Level", data.waterLevel))
            }.interpolationMethod(.catmullRom)
                .foregroundStyle(by: .value("Type", "previous".localizedLowercase))
            
            ForEach(expected) { data in
                LineMark(x: .value("Time", data.atDateTime),
                         y: .value("Level", data.waterLevel))
            }.interpolationMethod(.catmullRom)
                .foregroundStyle(by: .value("Type", "expected"))

            ForEach(astro) { data in
                LineMark(x: .value("Time", data.atDateTime),
                         y: .value("Level", data.waterLevel))
            }.interpolationMethod(.catmullRom)
                .foregroundStyle(by: .value("Type", "astro"))
        }
        .chartYAxis {
            AxisMarks(position: .leading)
        }
        .chartXAxis {
            AxisMarks(values: .stride(by: .hour)) { value in
                AxisGridLine()
                AxisTick()
                AxisValueLabel(format: .dateTime.hour())
            }
        }
    }
}

//struct TideCartData {
//    let name: String
//    let levels: [ExpectedWaterData]
//}
//
//struct TideChartDataView: View {
//    let data: [TideCartData]
//    
//    var body: some View {
//        Chart {
//            ForEach(data, id: \.name) { level in
//                ForEach(level) { data in
//                    LineMark(x: .value("Time", data.atDateTime),
//                             y: .value("Level", data.waterLevel))
//                }.interpolationMethod(.catmullRom)
//                    .foregroundStyle(by: .value("Type", level.name))
//            }
//        }
//        .chartYAxis {
//            AxisMarks(position: .leading)
//        }
//        .chartXAxis {
//            AxisMarks(values: .stride(by: .hour)) { value in
//                AxisGridLine()
//                AxisTick()
//                AxisValueLabel(format: .dateTime.hour())
//            }
//        }
//    }
//}
