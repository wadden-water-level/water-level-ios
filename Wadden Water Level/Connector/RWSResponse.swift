//
//  WaterData.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 13/06/2021.
//

import Foundation

struct RWSWaterData: Hashable, Codable {
    var WaarnemingenLijst: [RWSMeasurements]
}

struct RWSMeasurements: Hashable, Codable {
    var Locatie: RWSLocation
    var MetingenLijst: [RWSMeasurementData]
}

struct RWSLocation: Hashable, Codable {
//    "Locatie_MessageID": 18741,
//    "Coordinatenstelsel": "25831",
//    "X": 712485.362427151,
//    "Y": 5922496.96218518,
//    "Naam": "Lauwersoog",
//    "Code": "LAUW"
    
    var X: Double
    var Y: Double
    var Code: String
}

struct RWSMeasurementData: Hashable, Codable {
    var Tijdstip: String
    var Meetwaarde: RWSMeasurement
}

struct RWSMeasurement: Hashable, Codable {
    var Waarde_Numeriek: Double
}
