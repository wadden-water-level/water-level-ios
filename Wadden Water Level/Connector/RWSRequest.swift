//
//  WaterDataRequest.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 13/06/2021.
//

import Foundation


struct RWSCurrentWaterLevelRequest: Hashable, Codable {
    var AquoPlusWaarnemingMetadataLijst: [RWSAquoPlusObservationMetadata]
    var LocatieLijst: [RWSLocation]
}

struct RWSAquoPlusObservationMetadata: Hashable, Codable {
    var AquoMetadata: RWSMetaData
}

struct RWSMetaData: Hashable, Codable {
    var Compartiment: RWSMetaDataValue
    var Eenheid: RWSMetaDataValue
    var Grootheid: RWSMetaDataValue
}

struct RWSMetaDataValue: Hashable, Codable {
    var Code: String
}

struct RWSExpectedWaterLevelRequest: Hashable, Codable {
    var Locatie: RWSLocation
    var AquoPlusWaarnemingMetadata: RWSAquoPlusObservationMetadata
    var Periode: RWSTimePeriod
}

struct RWSTimePeriod: Hashable, Codable {
    var Begindatumtijd: String
    var Einddatumtijd: String
}

//{"AquoPlusWaarnemingMetadataLijst":[
//    {
//        "AquoMetadata":{
//            "Compartiment":{"Code":"OW"},
//            "Eenheid":{"Code":"cm"},
//            "Grootheid":{"Code":"WATHTE"}
//        }
//    }
//    ],
//    "LocatieLijst":[{"X": 712485.362427151,"Y": 5922496.96218518,"Code": "LAUW"}]
//}
