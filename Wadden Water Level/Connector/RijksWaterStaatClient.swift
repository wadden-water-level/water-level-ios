//
//  RWSClient.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 13/06/2021.
//

import Foundation

class RijksWaterStaatClient {
    func getCurrentWaterLevel(forLocation: RWSLocation) async throws -> RWSWaterData {
        let rwsUrl = "https://waterwebservices.rijkswaterstaat.nl/ONLINEWAARNEMINGENSERVICES_DBO/OphalenLaatsteWaarnemingen"
        let url = URL(string: rwsUrl)!
        
        let requestObject = RWSCurrentWaterLevelRequest(AquoPlusWaarnemingMetadataLijst: [RWSAquoPlusObservationMetadata(AquoMetadata: RWSMetaData(Compartiment: RWSMetaDataValue(Code: "OW"), Eenheid: RWSMetaDataValue(Code: "cm"), Grootheid: RWSMetaDataValue(Code: "WATHTE")))], LocatieLijst: [forLocation])
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        do {
            let httpBody = try JSONEncoder().encode(requestObject)
            request.httpBody = httpBody
        } catch let jsonError {
            print("Error while creating request body: " + jsonError.localizedDescription)
        }
        
        let (data, response) = try await URLSession.shared.data(for: request)

        let waterData = try JSONDecoder().decode(RWSWaterData.self, from: data)
        
        return waterData
    }
    
    func getPreviousWaterLevel(forLocation: RWSLocation, startDate: String, endDate: String) async throws -> RWSWaterData {
        return try await getPeriodWaterLevel(grootheid: "WATHTE",
                            forLocation: forLocation,
                            startDate: startDate,
                            endDate: endDate)
    }
    
    func getExpectedWaterLevel(forLocation: RWSLocation, startDate: String, endDate: String) async throws -> RWSWaterData {
        return try await getPeriodWaterLevel(grootheid: "WATHTEVERWACHT",
                            forLocation: forLocation,
                            startDate: startDate,
                            endDate: endDate)
    }
    
    func getAstroWaterLevel(forLocation: RWSLocation, startDate: String, endDate: String) async throws -> RWSWaterData {
        return try await getPeriodWaterLevel(grootheid: "WATHTEASTRO",
                            forLocation: forLocation,
                            startDate: startDate,
                            endDate: endDate)
    }

    func getPeriodWaterLevel(grootheid: String, forLocation: RWSLocation, startDate: String, endDate: String) async throws -> RWSWaterData  {
        let rwsUrl = "https://waterwebservices.rijkswaterstaat.nl/ONLINEWAARNEMINGENSERVICES_DBO/OphalenWaarnemingen"
        guard let url = URL(string: rwsUrl) else { print("RWS URL is invalid"); throw RijksWaterStaatClientError.invalidUrl }

        let requestObject = RWSExpectedWaterLevelRequest(
            Locatie: forLocation,
            AquoPlusWaarnemingMetadata: RWSAquoPlusObservationMetadata(
                AquoMetadata: RWSMetaData(
                    Compartiment: RWSMetaDataValue(Code: "OW"),
                    Eenheid: RWSMetaDataValue(Code: "cm"),
                    Grootheid: RWSMetaDataValue(Code: grootheid)
                )
            ),
            Periode: RWSTimePeriod(Begindatumtijd: startDate, Einddatumtijd: endDate)
        )

        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
                
        do {
            let httpBody = try JSONEncoder().encode(requestObject)
            request.httpBody = httpBody

            let (data, response) = try await URLSession.shared.data(for: request)

            if(response.getStatusCode() != 200) {
                print("Error response: \(response.getStatusCode() ?? 0)")
                throw RijksWaterStaatClientError.invalidResponse
            }
            
            let waterData = try JSONDecoder().decode(RWSWaterData.self, from: data)
            return waterData
        } catch EncodingError.invalidValue(let value, let jsonError) {
            print("Error while creating request body: " + jsonError.debugDescription)
            throw RijksWaterStaatClientError.invalidRequest
        } catch {
            print("Error retrieving data")
            throw RijksWaterStaatClientError.failureGettingData
        }
    }
}

extension URLResponse {
    func getStatusCode() -> Int? {
        if let httpResponse = self as? HTTPURLResponse {
            return httpResponse.statusCode
        }
        return nil
    }
}

enum RijksWaterStaatClientError: Error {
    case invalidUrl
    case invalidRequest
    case invalidResponse
    case failureGettingData
}
