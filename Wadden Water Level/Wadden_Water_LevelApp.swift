//
//  Wadden_Water_LevelApp.swift
//  Wadden Water Level
//
//  Created by Hugo Witsel on 12/06/2021.
//

import SwiftUI

@main
struct Wadden_Water_LevelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
