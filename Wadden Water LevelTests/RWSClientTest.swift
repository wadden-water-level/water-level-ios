//
//  RWSClientTest.swift
//  Wadden Water LevelTests
//
//  Created by Hugo Witsel on 13/06/2021.
//

import XCTest

class RWSClientTest: XCTestCase {



    func testExample() async throws {
        let result = try await RijksWaterStaatClient().getCurrentWaterLevel(forLocation: RWSLocation(X:712485.362427151, Y:5922496.96218518, Code:"LAUW"))

        XCTAssertEqual(result.WaarnemingenLijst[0].Locatie.Code, "LAUW")
    }

}
