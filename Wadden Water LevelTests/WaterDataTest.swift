//
//  WaterData.swift
//  Wadden Water LevelTests
//
//  Created by Hugo Witsel on 13/06/2021.
//

import XCTest

class WaterDataTest: XCTestCase {

    func testDeserialize() throws {
        XCTAssertEqual(observations.WaarnemingenLijst[0].Locatie.Code, "LAUW")
        XCTAssertEqual(observations.WaarnemingenLijst[0].MetingenLijst[0].Meetwaarde.Waarde_Numeriek, 27.0)
    }
    
    func testDateFormatting() throws {
        let timestamp = "2022-05-20T07:40:00.000+01:00"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        let measureTime = dateFormatter.date(from: timestamp)!

        
        let printFormatter = DateFormatter()
        printFormatter.locale = Locale(identifier: "nl_NL")
        printFormatter.setLocalizedDateFormatFromTemplate("dd MMM HH:mm")

        XCTAssertEqual(printFormatter.string(from: measureTime), "20 mei 08:40")

        
        printFormatter.locale = Locale(identifier: "en_US")
        printFormatter.setLocalizedDateFormatFromTemplate("dd MMM HH:mm")
        XCTAssertEqual(printFormatter.string(from: measureTime), "May 20, 08:40")

    }
    
    func testDateFormatting2() throws {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSZZZZZ"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"

        let date = dateFormatterGet.date(from: "2016-02-29T12:24:26.123+01:00")!
        print(dateFormatterPrint.string(from: date))

    }
}
